import * as React from 'react'
import Layout from '../components/layout'

// markup
const IndexPage = () => {
  return (
    <Layout pageTitle="Home Page">
	  <p>Home page</p>
    </Layout>
  )
}

export default IndexPage
